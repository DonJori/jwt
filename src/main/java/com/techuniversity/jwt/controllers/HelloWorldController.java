package com.techuniversity.jwt.controllers;

import com.techuniversity.jwt.components.JWTBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("techu/jwt")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class HelloWorldController {
    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping(path = "/tokenget")
    public String getToken(@RequestParam(value = "nombre", defaultValue = "Techu") String name){
        return jwtBuilder.generateToken(name,"admin");
    }

    @GetMapping(path = "/hola", headers = {"Authorization"})
    public String hola(@RequestHeader("Authorization") String token) {
        String userId;

        try {
            userId = jwtBuilder.validarToken(token);
        } catch (Exception ex) {
            return ex.getMessage();
        }

        return String.format("Saludos para %s desde Demo JWT", userId);
    }

}
